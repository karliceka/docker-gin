## 20220406
- přidání ukázkového `.env`
- provázání do externího `.env`
    - přístupové údaje
    - názvy kontejnerů
    - nastavení db
    - porty
- odstranění `chronograph`
